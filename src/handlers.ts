import * as hapi from "@hapi/hapi";
import * as bcrypt from "bcrypt";

import {Users} from "./users";

const users: Users = new Users(__dirname + '/users.json');

export async function validate(request, session) {
    const account = await users.findByUsername(session.username);
    if (!account) {
        return { valid: false };
    }
    return { valid: true, credentials: account };
}

export async function  signin(request: hapi.Request, h: hapi.Reply) {
    const { username, password } = request.payload;
    const account = users.findByUsername(username);
    if (!account || !(await bcrypt.compare(password, account.password))) {
        return h.view('signin', {message: 'Wrong username or password!'});
    }
    request.cookieAuth.set({ username: account.username });
    return h.redirect('/');
}

export async function  signup(request: hapi.Request, h: hapi.Reply) {
    const { username, password, password2 } = request.payload;
    const account = users.findByUsername(username);
    if (account) {
        return h.view('signup', { message: 'Username is already taken!' });
    }
    if (password != password2) {
        return h.view('signup', { message: 'Passwords do not match!' });
    }
    users.createUser({ username, password: await bcrypt.hash(password, 10) });
    return h.redirect('/signin');
}