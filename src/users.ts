'use strict';

import { readFileSync, writeFileSync } from "fs";

export type User = {
    username: string
    password: string
}

export class Users {

    private filename: string;
    private users: User[];

    constructor (filename: string) {
        this.filename = filename;
        const json: string = readFileSync(filename, 'utf-8');
        this.users = JSON.parse(json) as User[];
    }

    public findByUsername (username: string): User {
        return this.users.find((u: User) => u.username.toLowerCase() === username.toLowerCase());
    }

    public createUser (user: User) {
        if (!this.findByUsername(user.username))
            this.users.push(user);
        writeFileSync(this.filename, JSON.stringify(this.users));
    }

}
