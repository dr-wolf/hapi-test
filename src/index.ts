'use strict';

import * as hapi from '@hapi/hapi';
import * as handlebars from 'handlebars';

import * as handlers from './handlers';

(async function () {

    const server: hapi.Server = new hapi.Server({ port: 3000, host: 'localhost' });

    await server.register(require('@hapi/cookie'));
    await server.register(require('@hapi/vision'));

    server.auth.strategy('session', 'cookie', {
        cookie: {
            name: 'sid-example',
            password: 'Сорок тысяч обезьян в жопу сунули банан.',
            isSecure: false
        },
        redirectTo: '/signin',
        validateFunc: handlers.validate
    });

    server.views({
        engines: {
            html: handlebars
        },
        relativeTo: __dirname,
        path: 'templates',
        layout: true,
        layoutPath: 'templates'
    });

    server.auth.default('session');

    server.route([
        {
            method: "GET",
            path: "/",
            handler: (request: hapi.Request, h: hapi.Reply) => h.view('index', { username: request.auth.credentials.username })
        },
        {
            method: 'GET',
            path: '/signin',
            handler: (request: hapi.Request, h: hapi.Reply) =>  h.view('signin'),
            options: {
                auth: false
            }
        },
        {
            method: 'GET',
            path: '/signup',
            handler: (request: hapi.Request, h: hapi.Reply) =>  h.view('signup'),
            options: {
                auth: false
            }
        },
        {
            method: 'POST',
            path: '/signin',
            handler: handlers.signin,
            options: {
                auth: {
                    mode: 'try'
                }
            }
        },
        {
            method: 'POST',
            path: '/signup',
            handler: handlers.signup,
            options: {
                auth: {
                    mode: 'try'
                }
            }
        }
    ]);

    await server.start();

    console.log('Server is running at :3000');
})();
